#!/bin/bash

# Create a build directory
mkdir -p build
cd build

# Run CMake to configure the project and generate a native build system, output more information
cmake .. -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON

# Build the project with verbose output
make VERBOSE=1

# Return to the original directory
cd ..

echo "Build completed. The executable can be found in the 'build' directory."
