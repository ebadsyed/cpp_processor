#include <opencv2/opencv.hpp>
#include <iostream>

/**
 * Pads an image to meet specified minimum dimensions and optionally ensure that dimensions are divisible by given values.
 * It also allows for specific positioning of the padded image within the new dimensions.
 *
 * @param img The input image to be padded.
 * @param minHeight The minimum height the image should have after padding.
 * @param minWidth The minimum width the image should have after padding.
 * @param padHeightDivisor If set, adjusts the total height (image height + padding) to be divisible by this value. Default is 1.
 * @param padWidthDivisor If set, adjusts the total width (image width + padding) to be divisible by this value. Default is 1.
 * @param position Specifies the position where the image should be placed within the padded area. Options include 'center', 
 *                 'top_left', 'top_right', 'bottom_left', 'bottom_right'. Default is 'center', which evenly distributes padding.
 * @param borderMode Specifies the border mode to use when applying padding. Uses OpenCV border types like cv::BORDER_CONSTANT 
 *                   for padding with a solid color or cv::BORDER_REFLECT, etc. Default is cv::BORDER_CONSTANT.
 * @param value The color used for padding if borderMode is cv::BORDER_CONSTANT. Default is black (Scalar(0,0,0)).
 *
 * @return The padded image as a cv::Mat object.
 */
cv::Mat padIfNeeded(const cv::Mat &img, int minHeight, int minWidth, 
                 int padHeightDivisor = 1, int padWidthDivisor = 1, 
                 std::string position = "center", 
                 int borderMode = cv::BORDER_CONSTANT, cv::Scalar value = cv::Scalar(0, 0, 0)) {
    int width = img.cols;
    int height = img.rows;
    int padLeft = 0, padRight = 0, padTop = 0, padBottom = 0;

    // Calculate the target aspect ratio based on provided minimum dimensions
    double targetRatio = double(minWidth) / minHeight;
    double imageRatio = double(width) / height;

    // Adjust width or height to maintain the aspect ratio
    if (imageRatio < targetRatio) {
        // Image is too tall, increase width
        int newWidth = int(height * targetRatio);
        int padWidth = newWidth - width;
        padLeft = padWidth / 2;
        padRight = padWidth - padLeft;
    } else {
        // Image is too wide, increase height
        int newHeight = int(width / targetRatio);
        int padHeight = newHeight - height;
        padTop = padHeight / 2;
        padBottom = padHeight - padTop;
    }

    // Adjust padding based on specified position
    if (position == "top_left") {
        padBottom += padTop; padTop = 0;
        padRight += padLeft; padLeft = 0;
    } else if (position == "top_right") {
        padBottom += padTop; padTop = 0;
        padLeft += padRight; padRight = 0;
    } else if (position == "bottom_left") {
        padTop += padBottom; padBottom = 0;
        padRight += padLeft; padLeft = 0;
    } else if (position == "bottom_right") {
        padTop += padBottom; padBottom = 0;
        padLeft += padRight; padRight = 0;
    } // 'center' is handled by default symmetric padding

    // Ensure final dimensions are divisible by specified divisors
    if ((width + padLeft + padRight) % padWidthDivisor != 0) {
        int extra = padWidthDivisor - ((width + padLeft + padRight) % padWidthDivisor);
        padRight += extra;
    }
    if ((height + padTop + padBottom) % padHeightDivisor != 0) {
        int extra = padHeightDivisor - ((height + padTop + padBottom) % padHeightDivisor);
        padBottom += extra;
    }

    // Apply padding using specified border mode and value
    cv::Mat paddedImg;
    cv::copyMakeBorder(img, paddedImg, padTop, padBottom, padLeft, padRight, borderMode, value);
    return paddedImg;
}

// Assuming the function padIfNeeded is defined in the same way as before.

struct TestImage {
    std::string filename;
    int targetWidth;
    int targetHeight;
};

int main() {
    std::vector<std::string> filenames = {
        "1-1.jpg", 
        "4-3.jpg", 
        "16-9.jpg", 
        "21-9.jpg", 
        "3-4.jpg"
    };

    std::string basePath = "test_images/";

    for (const auto& filename : filenames) {
        std::string fullPath = basePath + filename;
        cv::Mat image = cv::imread(fullPath);

        if (image.empty()) {
            std::cerr << "Failed to load image: " << filename << std::endl;
        } else {
            std::cout << "Dimensions of " << filename << ": " 
                      << image.cols << "x" << image.rows << " (Width x Height)" << std::endl;

            // Apply padding (example target dimensions and settings)
            int targetWidth = 1024; // Target width to adjust aspect ratio
            int targetHeight = 1792; // Target height to adjust aspect ratio
            cv::Mat paddedImage = padIfNeeded(image, targetHeight, targetWidth, 1, 1, "center", cv::BORDER_CONSTANT, cv::Scalar(0, 0, 0));
            std::string outputFilename = "padded_" + filename;
            cv::imwrite(outputFilename, paddedImage);

            std::cout << "Processed and saved: " << outputFilename << std::endl;
        }
    }

    return 0;
}